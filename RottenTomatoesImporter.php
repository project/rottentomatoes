<?php

/**
 * @file
 * Contains RottenTomatoesImporter.
 */

/**
 * Base class for importing data from RottenTomatoes to one or more nodes.
 */
abstract class RottenTomatoesImporter {

  /**
   * Returns the mapping operations for each field.
   *
   *
   *
   * Some XML documents contain multiple items that should each be imported to
   * a node; in these cases, the XML often has data at the root level or some
   * other parent level element that should be included on a node. This suggests
   * MULTIPLE sets of mappings, one for each level of the document that has
   * necessary information.
   *
   * @return array
   *   An array of callables that return a property or field value, keyed by
   *   property or field name. The keys of the array are the names of fields
   *   in the node to be imported to.  The values are the callable that will
   *   generate that field structure.
   */
  abstract public function getMapping();

  /**
   * Map data from a Rotten Tomatoes document to one or more nodes.
   *
   * @param mixed $source
   *   The incoming data to turn into nodes. This could be a single object or
   *   a collection, depending on the individual implementation.
   *
   * @return array
   *   An array of unsaved node objects.
   */
  abstract public function import($source);

  /**
   * Import data to one or more nodes.
   *
   * @param mixed $source
   *   The incoming data to turn into nodes. This could be a single object or
   *   a collection, depending on the individual implementation.
   *
   * @return array
   *   An array of saved node objects.
   */
  public function runImport($source) {
    // Map the document to node objects.
    $nodes = $this->import($source);

    // Save the nodes.
    foreach ($nodes as $node) {
      $this->saveNode($node);
    }

    return $nodes;
  }

  /**
   * Apply mappings from the QueryPath object to a node.
   *
   * @param QueryPath $qp
   *   The XML source as a QueryPath object; this may be a subset of the full
   *   document.
   * @param stdClass $node
   *   A node object.
   * @param array $map
   *   An array that maps field names to functions for extracting field data
   *   from a QueryPath object. If this is not provided, the map array from
   *   FoxtelImporter::getMapping() will be used.
   *
   * @return stdClass
   *   The node object with properties and fields added or updated with values
   *   from the current XML source.
   */
  public function map($source, stdClass $node, $map = NULL) {

    // @todo: This is not a good approach.  We should instead be doing nested
    // separate import objects.
    if (!is_array($map)) {
      $map = $this->getMapping();
    }

    foreach ($map as $name => $callback) {
      $node->$name = $callback($source);
    }

    return $node;
  }

  /**
   * Load an existing node based on its node type and a field value.
   *
   * @param string $node_type
   *   The type of node to load.
   * @param string $field_name
   *   The name of the field to reference.
   * @param $value
   *   The field value to look for.
   *
   * @return stdClass
   *   The most recently created node with a matching field value.
   */
  public static function getNodeByField($node_type, $field_name, $value, $col = 'value') {
    $query = new EntityFieldQuery();

    // Prevent node_access alters from being applied to this query. This is an
    // internal lookup, and we need to be able to reliably update the same node.
    // This may not be necessary if the view_unpublished module was not
    // installed.
    // @see http://drupal.org/node/1597378
    $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');

    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $node_type)
      ->fieldCondition($field_name, $col, $value)
      ->propertyOrderBy('created', 'DESC')
      ->range(0, 1);

    $result = $query->execute();
    if (isset($result['node']) && ($nid = key($result['node']))) {
      $node = node_load($nid);

      return $node;
    }

    return FALSE;
  }

  /**
   * Load existing node based on node type and values from multiple fields.
   *
   * @param string $node_type
   *   The type of node to load.
   * @param array $fields
   *   Arryas of Key value pairs plus the name of the Drupal column
   * that contains the value.
   *
   * @return stdClass
   *   The most recently created node with a matching field value.
   */
  public static function getNodeByFields($node_type, $fields) {
    $query = new EntityFieldQuery();

    // Prevent node_access alters from being applied to this query. This is an
    // internal lookup, and we need to be able to reliably update the same node.
    // This may not be necessary if the view_unpublished module was not
    // installed.
    // @see http://drupal.org/node/1597378
    $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');

    // $field['col'] is typically 'value
    foreach($fields as $field) {
      $query->fieldCondition($field['field_name'], $field['col'], $field['value']);
    }
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $node_type)
      ->propertyOrderBy('created', 'DESC')
      ->range(0, 1);

    $result = $query->execute();
    if (isset($result['node']) && ($nid = key($result['node']))) {
      $node = node_load($nid);

      return $node;
    }

    return FALSE;
  }

  /**
   * Get a fresh new node object.
   *
   * @param string $node_type
   *   The type of node object to get.
   *
   * @return stdClass
   *   A new node object with the type, language, and uid set.
   */
  public static function getEmptyNode($node_type) {
    $node = (object) array(
      'type' => $node_type,
      'language' => LANGUAGE_NONE,
      'uid' => 1,
      'status' => 0,
    );
    return $node;
  }

  /**
   * Load or create a node.
   *
   * @see static::getNodeByField()
   * @see static::getEmptyNode()
   *
   * @return stdClass
   *   A node object.
   */
  public static function getNode($node_type, $field_name, $value, $col = 'value') {
    $node = static::getNodeByField($node_type, $field_name, $value, $col);
    if (!$node) {
      $node = static::getEmptyNode($node_type);
    }
    return $node;
  }

  /**
   * Save a node, and throw our own exception if node_save() fails.
   *
   * @param stdClass $node
   *   A node object.
   *
   * @throws FoxtelImporterSaveException
   */
  public function saveNode($node) {
    try {
      node_save($node);
    }
    catch (Exception $e) {
      // @todo Wrap with a better exception.
      throw $e;
    }
  }

  /**
   * Helper for generating structure for storing simple field values.
   *
   * @param $value
   *   A scalar value for the field.
   *
   * @return array
   *   The array structure for a simple field.
   */
  public function setBasicField($value) {
    return array(
      LANGUAGE_NONE => array(
        0 => array(
          'value' => $value,
        ),
      ),
    );
  }

  /**
   * Helper for generating structure for storing multi-value field values.
   *
   * @param array $value
   *   An array of scalar value for the field.
   *
   * @return array
   *   The array structure for a simple field.
   */
  public function setBasicFields(array $values) {
    $items = array();
    foreach ($values as $value) {
      $items[LANGUAGE_NONE][] = array('value' => $value);
    }
    return $items;
  }

  /**
   * Helper for generating structure for storing text field values.
   *
   * @param mixed $value
   *  The intended field value.  Single string or an array of strings.
   * @param string $format
   *  The machine name of a Drupal input format to use for the string. Defaults
   *  to plain text.
   *
   * @return array
   *  The array structure for a text field.
   *
   * @todo It would be better if $values was always an array,
   * but that requires
   * changes to all importers that use this method.  Leaving as a mixed variable
   * for now.
   */
  public function setTextField($values, $format = 'plain') {
    $field = array(LANGUAGE_NONE => array());
    if (is_array($values)) {
      foreach($values as $value) {
        $field[LANGUAGE_NONE][] = array(
          'value' => $value,
          'format' => $format,
        );
      }
    }
    else {
      $field[LANGUAGE_NONE][] = array(
        'value' => $values,
        'format' => $format,
      );
    }
    return $field;
  }

  /**
   * Helper for generating structure for storing entity reference field values.
   *
   * @param int $value
   *   An entity id.
   *
   * @return array
   *   The array structure for an entity reference field.
   *
   * @todo support for multiple values
   */
  public function setEntityRefField($value) {
    return array(
      LANGUAGE_NONE => array(
        0 => array(
          'target_id' => $value,
        ),
      ),
    );
  }

  /**
  * Helper function for generating structure for storing date field values.
  *
  * For Foxtel, timezone and offset are always the same.
  *
  * @param string $value
  *  A date string, which will be parsed by date_api's DateObject.
  *
  * @return array
  *   A field data array for a field of type 'date'.
  */
  public function setDateField($value) {
    $date = new DateObject($value);
    return array(
      LANGUAGE_NONE => array(
        0 => array(
          'value' => date_format($date, DATE_FORMAT_ISO),
          'timezone' => 'UTC',
          'offset' => '0',
        ),
      ),
    );
  }

  /**
   * Helper for generating structure for storing simple link values (url only).
   *
   * @param $value
   *   A scalar value for the field.
   *
   * @return array
   *   The array structure for a simple field.
   */
  public function setLinkField($value) {
    return array(
      LANGUAGE_NONE => array(
        0 => array(
          'url' => $value,
        ),
      ),
    );
  }
}

