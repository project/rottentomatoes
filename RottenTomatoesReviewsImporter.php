<?php

use PalantirNet\RottenTomatoes\Reviews;

/**
 * Importer for Rotten Tomatoes Review objects.
 */
class RottenTomatoesReviewsImporter extends RottenTomatoesImporter {

  /**
   * {@inheritdoc}
   *
   * This method imports multiple reviews at once.
   */
  public function import($source) {
    $nodes = array();

    // Get Published status based on default value for the node type, rt_review
    $status = 0;
    $nodetype_options = variable_get('node_options_rt_review');
    if (isset($nodetype_options) && in_array('status', $nodetype_options)) {
      $status = 1;
    }

    // source includes all reviews
    $data = $source->getData();

    // create a multikey ID to find an existing review node
    foreach ($data['reviews'] as $review) {
      $combined_id = array(
        array(
          'field_name' => 'field_rt_critic',
          'value' => $review['critic'],
          'col' => 'value'
        ),
        array(
          'field_name' => 'field_rt_review_date',
          'value' => $review['date'],
          'col' => 'value'
        ),
        array(
          'field_name' => 'field_rt_publication',
          'value' => $review['publication'],
          'col' => 'value'
        )
      );
      // search for matching rt review node or create a new one
      $node = static::getReviewNode('rt_review', $combined_id);

      // provide the rt_id for every review so they link them to rt_movie nodes
      $review['rt_id'] = $source->id();

      // status should match the default settings for the node type when
      // a new rt_review node is created; status is not affected by RT updates
      if (!isset($node->nid)) {
        $node->status = $status;
      }

      // map values
      $this->map($review, $node);
      $node->title = implode('_', array($review['critic'], $review['date'], $review['publication']));
      $nodes[] = $node;
    }

    return $nodes;
  }

  /**
   * Load or create a node.
   *
   * @see static::getNodeByField()
   * @see static::getEmptyNode()
   *
   * @return stdClass
   *   A node object.
   */
  public static function getReviewNode($node_type, $fields) {
    $node = static::getNodeByFields($node_type, $fields);
    if (!$node) {
      $node = static::getEmptyNode($node_type);
    }
    return $node;
  }



  /**
   * {@inheritdoc}
   */
  public function getMapping() {
    $importer = $this;

    // Critic
    $map['field_rt_critic'] = function(array $review) use ($importer) {
      return $importer->setBasicField(($review['critic']));
    };

    // Date of Review
    $map['field_rt_review_date'] = function(array $review) use ($importer) {
      return $importer->setBasicField(($review['date']));
    };

    // Freshness
    $map['field_rt_freshness'] = function(array $review) use ($importer) {
      return $importer->setBasicField(($review['freshness']));
    };

    // Publication where review was written
    $map['field_rt_publication'] = function(array $review) use ($importer) {
      return $importer->setBasicField(($review['publication']));
    };

    // Quote
    $map['field_rt_quote'] = function(array $review) use ($importer) {
      return $importer->setBasicField(($review['quote']));
    };

    // Link to the Full Review
    $map['field_rt_review_link'] = function(array $review) use ($importer) {
      $review_link = '';
      if (isset($review['links']['review'])) {
        $review_link = $review['links']['review'];
      }
      return $importer->setLinkField($review_link);
    };

    // Movie RT ID
    $map['field_rt_id'] = function(array $review) use ($importer) {
      return $importer->setBasicField($review['rt_id']);
    };

    return $map;
  }

}
