This module provides tools for connecting to and importing data from the
Rotten Tomatoes service.

Installation
------------
Before enabling the module, you must run "php composer.phar install" from within
the module's directory.  That will download the 3rd party dependencies this
module has, as well as setup the autoloader.  This module requires the
composer_autoload module, or its code will not load correctly.

Configuration
-------------
There is currently only a single configuration directive: The Rotten Tomatoes
API key.  That may be set at /admin/config/services/rotten-tomatoes.
Alternatively, add it to your $conf array in settings.php like so:

$conf['rotten_tomatoes_api_key'] = 'abcde';

