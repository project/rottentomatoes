<?php

use PalantirNet\RottenTomatoes\Movie;
use PalantirNet\RottenTomatoes\CastList;
use PalantirNet\RottenTomatoes\CastMember;

/**
 * Importer for Rotten Tomatoes Movie objects.
 */
class RottenTomatoesMovieImporter extends RottenTomatoesImporter {

  /**
   * {@inheritdoc}
   */
  public function import($source) {
    $node = static::getNode('rt_movie', 'field_rt_id', $source->id());
    $this->map($source, $node);
    $node->title = $source->title();

    // Get Published status based on default value for the node type, rt_movie
    $status = 0;
    $nodetype_options = variable_get('node_options_rt_movie');
    if (isset($nodetype_options) && in_array('status', $nodetype_options)) {
      $status = 1;
    }
    // status should match the default settings for the node type when
    // a new rt_review node is created; status is not affected by RT updates
    if (!isset($node->nid)) {
      $node->status = $status;
    }
    return array($node);
  }

  /**
   * Serializes a CastList object to the string format expected by the field.
   *
   * @param \PalantirNet\RottenTomatoes\CastList $cast_list
   *   The cast list object to serialize.
   * @return string
   *   The serialized cast list.
   */
  public function serializeCast(CastList $cast_list) {
    $lines = array();
    foreach ($cast_list as $cast_member) {
      $lines[] = $this->formatCastMember($cast_member);
    }
    return implode("\n", $lines);
  }

  /**
   * Serializes a CastMember to the format expected by the field.
   *
   * @param \PalantirNet\RottenTomatoes\CastMember $cast_member
   *   The cast member to serialize.
   * @return string
   *   The serialized cast member entry.
   */
  protected function formatCastMember(CastMember $cast_member) {
    $data = $cast_member->getData();
    $data += array('name' => '', 'characters' => array());
    return $data['name'] . '|' . implode(', ', $data['characters']);
  }

  /**
   * {@inheritdoc}
   */
  public function getMapping() {
    $importer = $this;
    $map['field_rt_abridged_cast'] = function(Movie $movie) use ($importer) {
      $value = $importer->serializeCast($movie->getAbbridgedCastList());
      return $importer->setBasicField($value);
    };
    $map['field_rt_abridged_directors'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      $directors = array_map(function($director) {
        return $director['name'];
      }, isset($data['abridged_directors']) ? $data['abridged_directors'] : array());
      return $importer->setBasicFields($directors);
    };
    $map['field_rt_critics_consensus'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['critics_consensus']) ? $data['critics_consensus'] : '');
    };
    $map['field_rt_full_cast'] = function(Movie $movie) use ($importer) {
      $value = $importer->serializeCast($movie->getFullCastList());
      return $importer->setBasicField($value);
    };
    $map['field_rt_genres'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicFields(isset($data['genres']) ? $data['genres'] : array());
    };
    $map['field_rt_id'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['id']) ? $data['id'] : '');
    };
    $map['field_rt_imdb_id'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['alternate_ids']['imdb']) ? $data['alternate_ids']['imdb'] : '');
    };
    $map['field_rt_mpaa_rating'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['mapp_rating']) ? $data['mpaa_rating'] : '');
    };
    $map['field_rt_ratings_audience_rating'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['ratings']['audience_rating']) ? $data['ratings']['audience_rating'] : '');
    };
    $map['field_rt_ratings_audience_score'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['ratings']['audience_score']) ? $data['ratings']['audience_score'] : '');
    };
    $map['field_rt_ratings_critics_rating'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['ratings']['critics_rating']) ? $data['ratings']['critics_rating'] : '');
    };
    $map['field_rt_ratings_critics_score'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['ratings']['critics_score']) ? $data['ratings']['critics_score'] : '');
    };
    $map['field_rt_release_dates'] = function(Movie $movie) use ($importer) {
      $values = array();
      $data = $movie->getData();
      foreach ($data['release_dates'] as $type => $year) {
        $values[] = "{$type}|{$year}";
      }
      return $importer->setBasicField(implode("\n", $values));
    };
    $map['field_rt_studio'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['studio']) ? $data['studio'] : '');
    };
    $map['field_rt_synopsis'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['synopsis']) ? $data['synopsis'] : '');
    };
    $map['field_rt_year'] = function(Movie $movie) use ($importer) {
      $data = $movie->getData();
      return $importer->setBasicField(isset($data['year']) ? $data['year'] : '');
    };

    return $map;
  }
}
