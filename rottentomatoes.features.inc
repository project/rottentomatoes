<?php
/**
 * @file
 * rottentomatoes.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rottentomatoes_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function rottentomatoes_node_info() {
  $items = array(
    'rt_movie' => array(
      'name' => t('Rotten Tomatoes Movie'),
      'base' => 'node_content',
      'description' => t('A movie imported from Rotten Tomatoes'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rt_review' => array(
      'name' => t('Rotten Tomatoes Review'),
      'base' => 'node_content',
      'description' => t('Imported Rotten Tomatoes Reviews are stored as individual nodes.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
